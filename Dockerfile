FROM node:latest

RUN mkdir opt/rehacked

RUN npm install webpack webpack-dev-server -g

CMD ["node"]