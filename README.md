# Instructions

### Prerequisites

[Docker](https://www.docker.com/)

### Build docker image

`$docker build -t rehacked .`

**Details:** we are building a docker image from our docker file and we are using the `-t` flag to tag it with the name rehacked the `.` means our current directory

### Run our docker image

* *nix Environments: `$docker run -it -p 8080:8080 -v /absolute/path/to/your/app:/opt/rehacked rehacked bin/bash`

* Windows Environments: `docker run -it -p 8080:8080 -v //c/absolute/path/to/app:/opt/rehacked rehacked bin/bash`

**Details:** we are running our `rehacked` docker image, the `-it` flag means its an interactive session, the `-p` flag specifies the `internal:external` ports we will be exposing on our container, the `-v` flag mounts an absolute directory in our host machine into the directory specified `host_dir:container_dir`, `rehacked` is the tag of our docker image, and `bin/bash` will open a bash session in our container.

### Change directory to where we mounted our host files

`$cd opt/rehacked`

### Install packages

* *nix Environments: `$npm install`

* Windows Environments: 
    * `$npm rebuild` - rebuilds node modules
    * `$npm install --no-bin-links`

### Run the application

`webpack-dev-server --config webpack.config.js --content-base build/ --inline --hot --host 0.0.0.0`

### Login to application

**Username:** grales@gmail.com

**Password:** password

### Docker VirtualBox Port Forward Fix

**Skip this section if you are using Docker Beta, otherwise proceed with these instructions**

If you are not using the VirtualBox version of Docker you need to port forward your webpack ports in order to access your app in your host.

1. Open VirtualBox
2. Select the **default** virtual machine and click **settings**
3. Select **Network** in the sidebar menu options
4. Click **Port Forwarding**
5. Click the **Add a New Port Forward button**
6. In the **Name** field type **Docker**, in **Host IP** type **127.0.0.1**, in **Host Port** and **Guest Port** type **8080**
7. Click **OK**
8. You should now be able to access your app via **http://localhost:8080**
